public with sharing class DataForSummary {
    @AuraEnabled(cacheable=true)
    public static Map<String, Integer> fetchSummarizedData(){
        Map<String, Integer> countMap = new Map <String,Integer>();

        //Here instead of 
        // Account = Use the Object which has the Multi Picklist 
        // MultiSelect__c = Use the multi Picklist whose values you want to track.
        Schema.DescribeFieldResult multiPickList = Account.MultiSelect__c.getDescribe();
        for (Schema.PicklistEntry p: multiPickList.getPicklistValues() ) {
            countMap.put(p.getValue(),0);
        }

        
        for (Account a: [SELECT Id, MultiSelect__c from Account WHERE MultiSelect__c !=null]) {
            String []  mvpsplit = a.MultiSelect__c.split(';');
            for (String s :  mvpsplit) {
                if (countMap.ContainsKey(s) ) {
                    countMap.put(s, countMap.get(s) + 1);
                }
            }
        }
        System.debug('Count Map '+countMap.keySet());
        return countMap;
    }
      
}