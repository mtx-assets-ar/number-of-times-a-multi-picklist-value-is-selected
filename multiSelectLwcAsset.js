import { LightningElement, track, wire} from 'lwc';
import fetchSummarizedData from '@salesforce/apex/DataForSummary.fetchSummarizedData';

export default class MultiSelectLwcAsset extends LightningElement {
    @track mapOfValues = [];

    @wire(fetchSummarizedData)
    mapOfData({data, error}) {
        if(data) {
            for(let key in data) {
                // Preventing unexcepted data
                if (data.hasOwnProperty(key)) { // Filtering the data in the loop
                    this.mapOfValues.push({value:data[key], key:key});
                }
            }
        }
        else if(error) {
            window.console.log(error);
        }
    }
}